<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_Seo
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
namespace BelVG\Seo\Api;

use BelVG\Seo\Api\Data\SeoItemInterface;

interface SeoItemRepositoryInterface
{
    /**
     * @param int $itemId
     * @return SeoItemInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById(int $itemId): SeoItemInterface;

    /**
     * @param int $entityId
     * @param int $entityTypeId
     * @param int|null $storeId
     * @return SeoItemInterface|false
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get(
        int $entityId,
        int $entityTypeId,
        ?int $storeId = null
    );

    /**
     * @param SeoItemInterface $seoItem
     * @return SeoItemInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function save(SeoItemInterface $seoItem): SeoItemInterface;

    /**
     * @param SeoItemInterface $seoItem
     * @return bool
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete(SeoItemInterface $seoItem): bool;
}
