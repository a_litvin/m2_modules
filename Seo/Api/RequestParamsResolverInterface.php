<?php

namespace BelVG\Seo\Api;

interface RequestParamsResolverInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return int
     */
    public function getEntityTypeId(): int;
}
