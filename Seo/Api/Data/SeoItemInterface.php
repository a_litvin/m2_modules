<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_Seo
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
namespace BelVG\Seo\Api\Data;

interface SeoItemInterface
{
    const ENTITY_TYPE_ID = 'entity_type_id';
    const CODE = 'code';
    const REDIRECT_URL = 'redirect_url';
    const CANONICAL = 'canonical';
    const CANONICAL_URL = 'canonical_url';
    const STORE_ID = 'store_id';

    const ENTITY_TYPE_PRODUCT = 1;
    const ENTITY_TYPE_CATEGORY = 2;
    const ENTITY_TYPE_CMS_PAGE = 3;

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get Entity ID
     *
     * @return int|null
     */
    public function getEntityId();

    /**
     * Set Entity ID
     *
     * @param int $entityId
     * @return $this
     */
    public function setEntityId(int $entityId);

    /**
     * Get Entity Type ID
     *
     * @return int|null
     */
    public function getEntityTypeId(): ?int;

    /**
     * Set Entity Type ID
     *
     * @param int $entityTypeId
     * @return SeoItemInterface
     */
    public function setEntityTypeId(int $entityTypeId): SeoItemInterface;

    /**
     * Get Response Code
     *
     * @return int|null
     */
    public function getCode(): ?int;

    /**
     * Set Response Code
     *
     * @param int $code
     * @return SeoItemInterface
     */
    public function setCode(int $code): SeoItemInterface;

    /**
     * Get Redirect Url
     *
     * @return string|null
     */
    public function getRedirectUrl(): ?string;

    /**
     * Set Redirect Url
     *
     * @param string $url
     * @return SeoItemInterface
     */
    public function setRedirectUrl(string $url): SeoItemInterface;

    /**
     * Get Canonical Tag Value
     *
     * @return int|null
     */
    public function getCanonical(): ?string;

    /**
     * Set Canonical Tag Value
     *
     * @param string $canonical
     * @return SeoItemInterface
     */
    public function setCanonical(string $canonical): SeoItemInterface;

    /**
     * Get Canonical Url
     *
     * @return string|null
     */
    public function getCanonicalUrl(): ?string;

    /**
     * Set Canonical Url
     *
     * @param string $url
     * @return SeoItemInterface
     */
    public function setCanonicalUrl(string $url): SeoItemInterface;

    /**
     * Get Store ID
     *
     * @return int
     */
    public function getStoreId(): int;

    /**
     * Set Store ID
     *
     * @param int $storeId
     * @return SeoItemInterface
     */
    public function setStoreId(int $storeId): SeoItemInterface;
}
