<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_Seo
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
namespace BelVG\Seo\Model;

use BelVG\Seo\Api\Data\SeoItemInterface;
use Magento\Framework\Model\AbstractModel;

class SeoItem extends AbstractModel implements SeoItemInterface
{
    /**
     * @var \BelVG\Seo\Api\CanonicalUrlResolverInterface
     */
    protected $canonicalUrlResolver = null;

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(\BelVG\Seo\Model\ResourceModel\SeoItem::class);
    }

    /**
     * @inheritDoc
     */
    public function getEntityTypeId(): ?int
    {
        return parent::getData(self::ENTITY_TYPE_ID);
    }

    /**
     * @inheritDoc
     */
    public function setEntityTypeId(int $entityTypeId): SeoItemInterface
    {
        return $this->setData(self::ENTITY_TYPE_ID, $entityTypeId);
    }

    /**
     * @inheritDoc
     */
    public function getCode(): ?int
    {
        return parent::getData(self::CODE);
    }

    /**
     * @inheritDoc
     */
    public function setCode(int $code): SeoItemInterface
    {
        return $this->setData(self::CODE, $code);
    }

    /**
     * @inheritDoc
     */
    public function getRedirectUrl(): ?string
    {
        return parent::getData(self::REDIRECT_URL);
    }

    /**
     * @inheritDoc
     */
    public function setRedirectUrl(string $url): SeoItemInterface
    {
        return $this->setData(self::REDIRECT_URL, $url);
    }

    /**
     * @inheritDoc
     */
    public function getCanonical(): ?string
    {
        return parent::getData(self::CANONICAL);
    }

    /**
     * @inheritDoc
     */
    public function setCanonical(string $canonical): SeoItemInterface
    {
        return $this->setData(self::CANONICAL, $canonical);
    }

    /**
     * @inheritDoc
     */
    public function getCanonicalUrl(): ?string
    {
        return parent::getData(self::CANONICAL_URL);
    }

    /**
     * @inheritDoc
     */
    public function setCanonicalUrl(string $url): SeoItemInterface
    {
        return $this->setData(self::CANONICAL_URL, $url);
    }

    /**
     * @inheritDoc
     */
    public function getStoreId(): int
    {
        return parent::getData(self::STORE_ID);
    }

    /**
     * @inheritDoc
     */
    public function setStoreId(int $storeId): SeoItemInterface
    {
        return $this->setData(self::STORE_ID, $storeId);
    }

    /**
     * @return \BelVG\Seo\Api\CanonicalUrlResolverInterface|null
     */
    public function getCanonicalUrlResolver(): ?\BelVG\Seo\Api\CanonicalUrlResolverInterface
    {
        if (null === $this->canonicalUrlResolver) {
            switch ($this->getEntityTypeId()) {
                case self::ENTITY_TYPE_PRODUCT:
                    $this->canonicalUrlResolver = \Magento\Framework\App\ObjectManager::getInstance()
                        ->get(\BelVG\Seo\Model\CanonicalUrlResolver\ProductCanonicalUrlResolver::class);
                    break;
                case self::ENTITY_TYPE_CATEGORY:
                    $this->canonicalUrlResolver = \Magento\Framework\App\ObjectManager::getInstance()
                        ->get(\BelVG\Seo\Model\CanonicalUrlResolver\CategoryCanonicalUrlResolver::class);
                    break;
                case self::ENTITY_TYPE_CMS_PAGE:
                    $this->canonicalUrlResolver = \Magento\Framework\App\ObjectManager::getInstance()
                        ->get(\BelVG\Seo\Model\CanonicalUrlResolver\CmsPageCanonicalUrlResolver::class);
                    break;
            }
        }

        return $this->canonicalUrlResolver;
    }
}
