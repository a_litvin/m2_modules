<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_Seo
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
namespace BelVG\Seo\Model\CanonicalUrlResolver;

class CategoryCanonicalUrlResolver extends AbstractCanonicalUrlResolver
{
    /**
     * @var \Magento\Catalog\Api\CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * Product constructor.
     *
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->categoryRepository = $categoryRepository;
        parent::__construct($storeManager);
    }

    public function execute(int $entityId): ?string
    {
        try {
            $currentStoreId = $this->storeManager->getStore()->getId();
            $category = $this->categoryRepository->get($entityId, $currentStoreId);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
            return null;
        }

        if (!$category || !$category->getIsActive()) {
            return null;
        }

        $stores = $this->getStoreIds($category);
        $storeId = $this->getStoreId($stores);
        $url = $category->getUrl();
        try {
            $url = str_replace($this->storeManager->getStore($currentStoreId)->getBaseUrl(), $this->storeManager->getStore($storeId)->getBaseUrl(), $url);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
        }

        return $url;
    }

    /**
     * @param \Magento\Catalog\Api\Data\CategoryInterface $category
     * @return array
     */
    protected function getStoreIds(\Magento\Catalog\Api\Data\CategoryInterface $category): array
    {
        return $category->getStoreIds();
    }
}
