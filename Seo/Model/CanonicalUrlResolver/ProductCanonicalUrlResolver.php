<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_Seo
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
namespace BelVG\Seo\Model\CanonicalUrlResolver;

use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;

class ProductCanonicalUrlResolver extends AbstractCanonicalUrlResolver
{
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * Product constructor.
     *
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->productRepository = $productRepository;
        parent::__construct($storeManager);
    }

    /**
     * @inheritDoc
     */
    public function execute(int $entityId): ?string
    {
        try {
            $storeId = $this->storeManager->getStore()->getId();
            $product = $this->productRepository->getById($entityId, false, $storeId);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
            return null;
        }

        if ($product->getStatus() == Status::STATUS_DISABLED || $product->getVisibility() == Visibility::VISIBILITY_NOT_VISIBLE) {
            return null;
        }

        $stores = $this->getStoreIds($product);
        $storeId = $this->getStoreId($stores);

        return $product->getUrlModel()->getUrl($product, ['_scope' => $storeId]);
    }

    /**
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return array
     */
    protected function getStoreIds(\Magento\Catalog\Api\Data\ProductInterface $product): array
    {
        return $product->getStoreIds();
    }
}
