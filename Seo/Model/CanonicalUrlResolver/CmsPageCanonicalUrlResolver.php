<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_Seo
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
namespace BelVG\Seo\Model\CanonicalUrlResolver;

class CmsPageCanonicalUrlResolver extends AbstractCanonicalUrlResolver
{
    /**
     * @var \Magento\Cms\Api\PageRepositoryInterface
     */
    protected $cmsRepository;

    /**
     * Product constructor.
     *
     * @param \Magento\Cms\Api\PageRepositoryInterface $cmsRepository
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Cms\Api\PageRepositoryInterface $cmsRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->cmsRepository = $cmsRepository;
        parent::__construct($storeManager);
    }

    /**
     * @inheritDoc
     */
    public function execute(int $entityId): ?string
    {
        try {
            $page = $this->cmsRepository->getById($entityId);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
            return null;
        }

        if (!$page || !$page->isActive()) {
            return null;
        }

        $stores = $this->getStoreIds($page);
        $storeId = $this->getStoreId($stores);

        try {
            $url = $this->storeManager->getStore($storeId)->getCurrentUrl(false);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
            $url = null;
        }

        return $url;
    }

    /**
     * @param \Magento\Cms\Api\Data\PageInterface $page
     * @return array
     */
    protected function getStoreIds(\Magento\Cms\Api\Data\PageInterface $page): array
    {
        return $page->getStores();
    }
}
