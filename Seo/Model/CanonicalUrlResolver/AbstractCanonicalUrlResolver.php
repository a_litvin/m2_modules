<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_Seo
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
namespace BelVG\Seo\Model\CanonicalUrlResolver;

abstract class AbstractCanonicalUrlResolver implements \BelVG\Seo\Api\CanonicalUrlResolverInterface
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Product constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;
    }

    /**
     * @param array $stores
     * @return int
     */
    protected function getStoreId(array $stores = []): int
    {
        $defaultStoreId = $this->storeManager->getDefaultStoreView()->getId();
        if (!$stores || !$stores[0] || in_array($defaultStoreId, $stores)) {
            $storeId = $defaultStoreId;
        } else {
            $storeId = $stores[0];
        }
        return $storeId;
    }
}
