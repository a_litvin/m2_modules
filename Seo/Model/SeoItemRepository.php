<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_Seo
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
namespace BelVG\Seo\Model;

class SeoItemRepository implements \BelVG\Seo\Api\SeoItemRepositoryInterface
{
    /**
     * @var SeoItem[]
     */
    protected $instances = [];

    /**
     * @var \BelVG\Seo\Model\ResourceModel\SeoItem
     */
    protected $resource;

    /**
     * @var SeoItemFactory
     */
    protected $seoItemFactory;

    /**
     * @param SeoItemFactory $seoItemFactory
     * @param \BelVG\Seo\Model\ResourceModel\SeoItem $resource
     */
    public function __construct(
        SeoItemFactory $seoItemFactory,
        \BelVG\Seo\Model\ResourceModel\SeoItem $resource
    ) {
        $this->seoItemFactory = $seoItemFactory;
        $this->resource = $resource;
    }

    /**
     * @inheritDoc
     */
    public function getById(int $itemId): \BelVG\Seo\Api\Data\SeoItemInterface
    {
        if (isset($this->instances[$itemId])) {
            return $this->instances[$itemId];
        }
        $seoItem = $this->seoItemFactory->create();
        $seoItem->load($itemId);
        if (!$seoItem->getId()) {
            throw new \Magento\Framework\Exception\NoSuchEntityException(__("The SEO Item doesn't exist."));
        }
        $this->instances[$itemId] = $seoItem;
        return $seoItem;
    }

    /**
     * @inheritDoc
     */
    public function get(int $entityId, int $entityTypeId, ?int $storeId = null)
    {
        $itemId = $this->resource->getByEntity($entityId, $entityTypeId, $storeId);
        return $this->getById($itemId);
    }

    /**
     * @inheritDoc
     */
    public function save(\BelVG\Seo\Api\Data\SeoItemInterface $seoItem): \BelVG\Seo\Api\Data\SeoItemInterface
    {
        try {
            $this->resource->save($seoItem);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(
                __('Could not save SEO Item for product: %1', $seoItem->getEntityId()),
                $exception
            );
        }
        unset($this->instances[$seoItem->getId()]);
        return $this->getById($seoItem->getId());
    }

    /**
     * @inheritDoc
     */
    public function delete(\BelVG\Seo\Api\Data\SeoItemInterface $seoItem): bool
    {
        try {
            $itemId = $seoItem->getId();
            $this->resource->delete($seoItem);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\StateException(
                __(
                    'Cannot delete SEO Item with id %1',
                    $seoItem->getId()
                ),
                $exception
            );
        }
        unset($this->instances[$itemId]);
        return true;
    }
}
