<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_Seo
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
namespace BelVG\Seo\Model\RequestParamsResolver;

use BelVG\Seo\Api\Data\SeoItemInterface;
use Magento\Framework\DataObject;

class ProductParamsResolver extends DataObject implements \BelVG\Seo\Api\RequestParamsResolverInterface
{
    /**
     * @inheritDoc
     */
    public function getId(): int
    {
        return parent::getData('id');
    }

    /**
     * @inheritDoc
     */
    public function getEntityTypeId(): int
    {
        if (null === parent::getData('entity_type_id')) {
            parent::setData('entity_type_id', SeoItemInterface::ENTITY_TYPE_PRODUCT);
        }
        return parent::getData('entity_type_id');
    }
}
