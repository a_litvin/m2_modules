<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_Seo
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
namespace BelVG\Seo\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class SeoItem extends AbstractDb
{
    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init('belvg_seo_entity', 'seo_entity_id');
    }

    /**
     * @param int $entityId
     * @param int $entityTypeId
     * @param int|null $storeId
     * @return int|false
     */
    public function getByEntity(
        int $entityId,
        int $entityTypeId,
        ?int $storeId
    ) {
        $connection = $this->getConnection();
        $select = $connection
            ->select()
            ->from($connection->getTableName($this->_mainTable), $this->_idFieldName)
            ->where('entity_id = :entity_id')
            ->where('entity_type_id = :entity_type_id');
        $bind = [
            ':entity_id' => (int) $entityId,
            ':entity_type_id' => (int) $entityTypeId
        ];
        if (null !== $storeId) {
            $select->where('(store_id = :store_id OR store_id = 0)');
            $bind[':store_id'] = (int) $storeId;
        }
        return $connection->fetchOne($select, $bind);
    }
}
