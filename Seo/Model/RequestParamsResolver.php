<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_Seo
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
namespace BelVG\Seo\Model;

class RequestParamsResolver
{
    /**
     * @var \BelVG\Seo\Api\RequestParamsResolverInterface
     */
    protected $resolver = null;

    /**
     * @param \Magento\Framework\App\RequestInterface $request
     * @return \BelVG\Seo\Api\RequestParamsResolverInterface|mixed|null
     */
    public function resolve(\Magento\Framework\App\RequestInterface $request): ?\BelVG\Seo\Api\RequestParamsResolverInterface
    {
        if (!method_exists($request, 'getControllerName')) {
            $this->resolver = null;
            return $this->resolver;
        }
        if (null !== $this->resolver) {
            return $this->resolver;
        }

        $controller = $request->getControllerName();
        switch ($controller) {
            case 'product':
                $this->resolver = \Magento\Framework\App\ObjectManager::getInstance()->get(\BelVG\Seo\Model\RequestParamsResolver\ProductParamsResolver::class);
                break;
            case 'category':
                $this->resolver = \Magento\Framework\App\ObjectManager::getInstance()->get(\BelVG\Seo\Model\RequestParamsResolver\CategoryParamsResolver::class);
                break;
            case 'page':
                $this->resolver = \Magento\Framework\App\ObjectManager::getInstance()->get(\BelVG\Seo\Model\RequestParamsResolver\CmsPageParamsResolver::class);
                break;
        }
        if (null !== $this->resolver) {
            $params = $request->getParams();
            $this->resolver->addData($params);
        }

        return $this->resolver;
    }
}
