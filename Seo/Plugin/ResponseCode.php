<?php

namespace BelVG\Seo\Plugin;

use BelVG\Seo\Api\Data\SeoItemInterface;

class ResponseCode
{
    /**
     * @var \BelVG\Seo\Model\RequestParamsResolver
     */
    private $paramsResolver;

    /**
     * @var \BelVG\Seo\Api\SeoItemRepositoryInterface
     */
    private $seoItemRepository;

    /**
     * @var \Magento\Framework\App\ResponseFactory
     */
    private $responseFactory;

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    private $redirectFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * ResponseCode constructor.
     * @param \BelVG\Seo\Model\RequestParamsResolver $paramsResolver
     * @param \BelVG\Seo\Api\SeoItemRepositoryInterface $seoItemRepository
     * @param \Magento\Framework\App\ResponseFactory $responseFactory
     * @param \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \BelVG\Seo\Model\RequestParamsResolver $paramsResolver,
        \BelVG\Seo\Api\SeoItemRepositoryInterface $seoItemRepository,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->paramsResolver = $paramsResolver;
        $this->seoItemRepository = $seoItemRepository;
        $this->responseFactory = $responseFactory;
        $this->redirectFactory = $redirectFactory;
        $this->storeManager = $storeManager;
    }

    public function afterDispatch(\Magento\Framework\App\Action\Action $subject, $result)
    {
        $paramsResolver = $this->paramsResolver->resolve($subject->getRequest());
        if (null === $paramsResolver) {
            return $result;
        }

        try {
            $entityId = $paramsResolver->getId();
            $entityTypeId = $paramsResolver->getEntityTypeId();
            $storeId = $this->storeManager->getStore()->getId();
            $seoItem = $this->seoItemRepository->get($entityId, $entityTypeId, $storeId);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
            return $result;
        }

        $responseCodeResult = $this->processResponseCode($seoItem);
        if ($responseCodeResult) {
            return $responseCodeResult;
        }

        return $result;
    }

    /**
     * @param SeoItemInterface $seoItem
     * @return false|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect
     */
    protected function processResponseCode(SeoItemInterface $seoItem)
    {
        $response = false;
        $code = $seoItem->getCode();

        switch ($code) {
            case \Zend\Http\Response::STATUS_CODE_301:
                $redirectUrl = $seoItem->getRedirectUrl();
                $response = $this->redirectFactory->create();
                $response->setPath($redirectUrl);
                break;
            case \Zend\Http\Response::STATUS_CODE_410:
                $response = $this->responseFactory->create();
                $response->setNoCacheHeaders();
                $response->setHttpResponseCode($code);
                break;
        }

        return $response;
    }
}
