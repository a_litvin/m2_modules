<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_Seo
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
namespace BelVG\Seo\Plugin;

use BelVG\Seo\Api\Data\SeoItemInterface;

class Canonical
{
    /**
     * @var \BelVG\Seo\Model\RequestParamsResolver
     */
    private $paramsResolver;

    /**
     * @var \BelVG\Seo\Api\SeoItemRepositoryInterface
     */
    private $seoItemRepository;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var \Magento\Framework\View\Page\Config
     */
    private $pageConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * Canonical constructor.
     *
     * @param \BelVG\Seo\Model\RequestParamsResolver $paramsResolver
     * @param \BelVG\Seo\Api\SeoItemRepositoryInterface $seoItemRepository
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\View\Page\Config $pageConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \BelVG\Seo\Model\RequestParamsResolver $paramsResolver,
        \BelVG\Seo\Api\SeoItemRepositoryInterface $seoItemRepository,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\View\Page\Config $pageConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->request = $request;
        $this->pageConfig = $pageConfig;
        $this->storeManager = $storeManager;
        $this->seoItemRepository = $seoItemRepository;
        $this->paramsResolver = $paramsResolver;
    }

    /**
     * @param \Magento\Framework\View\Result\Layout $subject
     * @param \Magento\Framework\App\ResponseInterface $response
     * @return array|void
     */
    public function beforeRenderResult(
        \Magento\Framework\View\Result\Layout $subject,
        \Magento\Framework\App\ResponseInterface $response
    ) {
        $paramsResolver = $this->paramsResolver->resolve($this->request);
        if (null === $paramsResolver) {
            return;
        }

        try {
            $entityId = $paramsResolver->getId();
            $entityTypeId = $paramsResolver->getEntityTypeId();
            $storeId = $this->storeManager->getStore()->getId();
            $seoItem = $this->seoItemRepository->get($entityId, $entityTypeId, $storeId);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
            return;
        }

        $url = $this->processCanonical($seoItem);
        if ($url) {
            $this->removeCanonical();
            $this->pageConfig->addRemotePageAsset($url, 'canonical', ['attributes' => ['rel' => 'canonical']]);
        }

        return [$response];
    }

    /**
     * @param SeoItemInterface $seoItem
     * @return false|string|null
     */
    protected function processCanonical(SeoItemInterface $seoItem)
    {
        $url = false;
        $canonical = $seoItem->getCanonical();

        switch ($canonical) {
            case 'canonical':
                $url = $seoItem->getCanonicalUrl();
                break;
            case 'self-referencing':
                $canonicalUrlResolver = $seoItem->getCanonicalUrlResolver();
                $url = $canonicalUrlResolver->execute($seoItem->getEntityId());
                break;
        }

        return $url;
    }

    /**
     * Remove old (default) canonical asset
     */
    private function removeCanonical(): void
    {
        $assets = $this->pageConfig->getAssetCollection();
        foreach ($assets->getAll() as $asset) {
            if ($asset->getContentType() === 'canonical') {
                $assets->remove($asset->getUrl());
            }
        }
    }
}
