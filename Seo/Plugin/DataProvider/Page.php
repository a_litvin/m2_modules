<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_Seo
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
namespace BelVG\Seo\Plugin\DataProvider;

use BelVG\Seo\Api\Data\SeoItemInterface;

class Page
{
    /**
     * @var \BelVG\Seo\Api\SeoItemRepositoryInterface
     */
    private $seoItemRepository;

    /**
     * PageRepository constructor.
     * @param \BelVG\Seo\Api\SeoItemRepositoryInterface $seoItemRepository
     */
    public function __construct(
        \BelVG\Seo\Api\SeoItemRepositoryInterface $seoItemRepository
    ) {
        $this->seoItemRepository = $seoItemRepository;
    }

    /**
     * @param \Magento\Cms\Model\Page\DataProvider $subject
     * @param array $result
     * @return array
     */
    public function afterGetData(
        \Magento\Cms\Model\Page\DataProvider $subject,
        array $result
    ): array {
        foreach ($result as $pageId => $data) {
            try {
                $seoItem = $this->seoItemRepository->get($pageId, SeoItemInterface::ENTITY_TYPE_CMS_PAGE);
            } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
                continue;
            }
            $seoItemData = [
                'response_code' => $seoItem->getCode(),
                'redirect_url' => $seoItem->getRedirectUrl(),
                'canonical' => $seoItem->getCanonical(),
                'canonical_url' => $seoItem->getCanonicalUrl()
            ];
            $result[$pageId] = $data + $seoItemData;
        }
        return $result;
    }
}
