<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_Seo
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
namespace BelVG\Seo\Observer\Backend;

use BelVG\Seo\Api\Data\SeoItemInterface;
use Magento\Framework\Event\Observer;

class CatalogCategorySaveAfterObserver extends AbstractSaveAfterObserver implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        $params = $this->request->getParams();
        /** @var \Magento\Catalog\Api\Data\CategoryInterface */
        $category = $observer->getCategory();
        $categoryId = $category->getId();
        $storeId = $category->getStoreId();

        try {
            $this->processSeoItem($categoryId, SeoItemInterface::ENTITY_TYPE_CATEGORY, $storeId, $params);
        } catch (\Magento\Framework\Exception\StateException | \Magento\Framework\Exception\LocalizedException $exception) {
        }
    }
}
