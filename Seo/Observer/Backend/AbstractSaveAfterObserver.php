<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_Seo
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
namespace BelVG\Seo\Observer\Backend;

abstract class AbstractSaveAfterObserver
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \BelVG\Seo\Api\SeoItemRepositoryInterface
     */

    private $seoItemRepository;
    /**
     * @var \BelVG\Seo\Model\SeoItemFactory
     */
    private $seoItemFactory;

    /**
     * AbstractSaveAfterObserver constructor.
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \BelVG\Seo\Api\SeoItemRepositoryInterface $seoItemRepository
     * @param \BelVG\Seo\Model\SeoItemFactory $seoItemFactory
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \BelVG\Seo\Api\SeoItemRepositoryInterface $seoItemRepository,
        \BelVG\Seo\Model\SeoItemFactory $seoItemFactory
    ) {
        $this->request = $request;
        $this->seoItemRepository = $seoItemRepository;
        $this->seoItemFactory = $seoItemFactory;
    }

    /**
     * @param int $entityId
     * @param int $entityTypeId
     * @param int $storeId
     * @param array $params
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function processSeoItem(int $entityId, int $entityTypeId, int $storeId, array $params): void
    {
        if (empty($params)) {
            return;
        }

        try {
            $seoItem = $this->seoItemRepository->get($entityId, $entityTypeId, $storeId);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
            $seoItem = $this->seoItemFactory->create();
            $seoItem->setEntityId($entityId)->setEntityTypeId($entityTypeId)->setStoreId($storeId);
        }

        $responseCode = $params['response_code'];
        $canonical = $params['canonical'];

        if ($responseCode === '0' && $canonical === '0') {
            $this->seoItemRepository->delete($seoItem);
            return;
        }

        $seoItem
            ->setCode($responseCode)
            ->setCanonical($canonical);
        if (($redirectUrl = $params['redirect_url'])) {
            $seoItem->setRedirectUrl($redirectUrl);
        }
        if (($canonicalUrl = $params['canonical_url'])) {
            $seoItem->setCanonicalUrl($canonicalUrl);
        }
        $this->seoItemRepository->save($seoItem);
    }
}
