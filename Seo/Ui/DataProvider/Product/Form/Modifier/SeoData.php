<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_Seo
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
namespace BelVG\Seo\Ui\DataProvider\Product\Form\Modifier;

use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Fieldset;
use Zend\Http\Response;

class SeoData extends \Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier
{
    /**
     * @var \Magento\Catalog\Model\Locator\LocatorInterface
     */
    protected $locator;

    /**
     * @var \BelVG\Seo\Api\SeoItemRepositoryInterface
     */
    protected $seoItemRepository;

    /**
     * SeoData constructor.
     * @param \Magento\Catalog\Model\Locator\LocatorInterface $locator
     * @param \BelVG\Seo\Api\SeoItemRepositoryInterface $seoItemRepository
     */
    public function __construct(
        \Magento\Catalog\Model\Locator\LocatorInterface $locator,
        \BelVG\Seo\Api\SeoItemRepositoryInterface $seoItemRepository
    ) {
        $this->locator = $locator;
        $this->seoItemRepository = $seoItemRepository;
    }

    /**
     * @inheritDoc
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * @inheritDoc
     */
    public function modifyMeta(array $meta)
    {
        $product = $this->locator->getProduct();
        try {
            $seoItem = $this->seoItemRepository->get($product->getId(), 1, $product->getStoreId());
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
        }

        $meta['belvg_seo'] = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('BelVG SEO'),
                        'sortOrder' => 100,
                        'collapsible' => true,
                        'componentType' => Fieldset::NAME,
                        'dataScope' => 'data.belvg_seo'
                    ]
                ]
            ],
            'children' => [
                'response_code' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'formElement' => 'select',
                                'componentType' => Field::NAME,
                                'options' => [
                                    ['value' => '0', 'label' => 'Default'],
                                    ['value' => (string) Response::STATUS_CODE_301, 'label' => (string) Response::STATUS_CODE_301],
                                    ['value' => (string) Response::STATUS_CODE_410, 'label' => (string) Response::STATUS_CODE_410],
                                ],
                                'visible' => 1,
                                'required' => 0,
                                'label' => __('Response code'),
                                'value' => isset($seoItem) ? $seoItem->getCode() : 0
                            ]
                        ]
                    ]
                ],
                'redirect_url' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'formElement' => 'input',
                                'componentType' => Field::NAME,
                                'required' => 1,
                                'label' => __('Redirect url'),
                                'value' => isset($seoItem) ? $seoItem->getRedirectUrl() : ''
                            ]
                        ]
                    ]
                ],
                'canonical' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'formElement' => 'select',
                                'componentType' => Field::NAME,
                                'options' => [
                                    ['value' => '0', 'label' => 'Default'],
                                    ['value' => 'self-referencing', 'label' => 'Self-referencing'],
                                    ['value' => 'canonical', 'label' => 'Canonical'],
                                ],
                                'visible' => 1,
                                'required' => 0,
                                'label' => __('Canonical Tag Value'),
                                'value' => isset($seoItem) ? $seoItem->getCanonical() : 0
                            ]
                        ]
                    ]
                ],
                'canonical_url' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'formElement' => 'input',
                                'componentType' => Field::NAME,
                                'required' => 1,
                                'label' => __('Canonical url'),
                                'value' => isset($seoItem) ? $seoItem->getCanonicalUrl() : ''
                            ]
                        ]
                    ]
                ]
            ]
        ];

        return $meta;
    }
}
