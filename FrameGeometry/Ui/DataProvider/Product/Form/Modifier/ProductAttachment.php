<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_FrameGeometry
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

declare(strict_types=1);

namespace BelVG\FrameGeometry\Ui\DataProvider\Product\Form\Modifier;

use BelVG\FrameGeometry\Setup\InstallData;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;

class ProductAttachment extends AbstractModifier
{
    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param ArrayManager $arrayManager
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(ArrayManager $arrayManager, StoreManagerInterface $storeManager)
    {
        $this->arrayManager = $arrayManager;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritDoc}
     */
    public function modifyData(array $data): array
    {
        return $data;
    }

    /**
     * {@inheritDoc}
     */
    public function modifyMeta(array $meta): array
    {
        $fieldCode = InstallData::FRAME_GEOMETRY_ATTRIBUTE_NAME;
        $elementPath = $this->arrayManager->findPath($fieldCode, $meta, null, 'children');

        if (!$elementPath) {
            return $meta;
        }

        $containerPath = $this->arrayManager->findPath(static::CONTAINER_PREFIX . $fieldCode, $meta, null, 'children');

        $mediaUrl =  $this->storeManager->getStore()
            ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);

        $meta = $this->arrayManager->merge(
            $containerPath,
            $meta,
            [
                'children'  => [
                    $fieldCode => [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'elementTmpl' => 'BelVG_FrameGeometry/element/product-attachment',
                                    'media_url' => $mediaUrl,
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        );

        return $meta;
    }
}
