<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_FrameGeometry
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

declare(strict_types=1);

namespace BelVG\FrameGeometry\Block\Product;

use Magento\Catalog\Block\Product\View;
use Magento\Framework\UrlInterface;

class Attachment extends View
{
    /**
     * @return string
     */
    public function getMediaUrl(): string
    {
        return $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
    }

    /**
     * @param $attachment
     *
     * @return string
     */
    public function getProductAttachmentUrl($attachment): string
    {
        return $this->getMediaUrl() . 'catalog/product/geometry' . $attachment;
    }
}
