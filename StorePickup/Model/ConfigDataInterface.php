<?php
namespace BelVG\StorePickup\Model;

/**
 * Class ConfigDataInterface
 * @package BelVG\StorePickup\Model
 */
interface ConfigDataInterface
{
    const CONFIG_XML_SP_PATH_PREFIX = 'carriers/belvg_storepickup/';

    const CONFIG_XML_REGION_DISPLAY = 'general/region/display_all';

    /**
     * @return array
     */
    public function getPickupAddress():array;

    /**
     * @return bool
     */
    public function isDisplayAllRegions():bool;
}
