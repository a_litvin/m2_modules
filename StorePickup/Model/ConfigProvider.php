<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_StorePickup
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
namespace BelVG\StorePickup\Model;

use \Magento\Store\Model\ScopeInterface;

class ConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{
    /**
     * @var \Magento\Directory\Model\RegionFactory
     */
    protected $_regionFactory;

    /**
     * @var ConfigDataInterface
     */
    private $configData;

    /**
     * ConfigProvider constructor.
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface         $storeManager
     * @param \Magento\Directory\Model\RegionFactory             $regionFactory
     * @param ConfigDataInterface                                $configData
     */
    public function __construct(
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \BelVG\StorePickup\Model\ConfigDataInterface $configData
    ) {
        $this->_regionFactory = $regionFactory;
        $this->configData = $configData;
    }

    /**
     * @inheritDoc
     */
    public function getConfig()
    {
        $pickupAddress = $this->configData->getPickupAddress();
        $config = [
            'storepickup_store_address' => $pickupAddress
        ];

        if ($this->configData->isDisplayAllRegions()) {
            $region = $this->getRegion();
            $config['storepickup_store_address']['region'] = [
                'region' => $region->getName(),
                'region_code' => $region->getCode(),
                'region_id' => $region->getId(),
            ];
        }

        $config['storepickup_store_address']['inline'] = sprintf(
            "%s %s, %s %s %s, %s, %s",
            $pickupAddress['firstname'],
            $pickupAddress['lastname'],
            $pickupAddress['postcode'],
            $pickupAddress['street_line1'],
            $pickupAddress['street_line2'],
            $pickupAddress['city'],
            $pickupAddress['telephone']
        );
        $config['storepickup_store_address']['belvg_storepickup'] = 1;

        return $config;
    }

    /**
     * @return \Magento\Directory\Model\Region
     */
    protected function getRegion()
    {
        $regionId = $this->configData->getStoreRegionId();
        return $this->_regionFactory->create()->load($regionId);
    }
}
