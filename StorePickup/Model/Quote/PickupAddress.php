<?php
namespace BelVG\StorePickup\Model\Quote;

/**
 * Class Address
 * @package BelVG\StorePickup\Model\Quote
 */
class PickupAddress extends AbstractAddress
{
    /**
     * @var \Magento\Checkout\Model\CompositeConfigProvider
     */
    private $config;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;

    /**
     * Address constructor.
     *
     * @param \Magento\Checkout\Model\ConfigDataInterface $config
     * @param \Magento\Checkout\Model\Session             $checkoutSession
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \BelVG\StorePickup\Model\ConfigDataInterface $config
    ) {
        $this->config = $config;
        parent::__construct($checkoutSession);
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function update()
    {
        $quote = $this->getQuote();
        $pickupAddress = $this->config->getPickupAddress();
        $quote->getShippingAddress()->addData(
            $pickupAddress
        );
    }
}
