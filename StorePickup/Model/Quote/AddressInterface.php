<?php
namespace BelVG\StorePickup\Model\Quote;

/**
 * Interface AddressInterface
 * @package BelVG\StorePickup\Model\Quote
 */
interface AddressInterface
{
    public function update();
}
