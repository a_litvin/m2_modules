<?php
namespace BelVG\StorePickup\Model\Quote;

/**'
 * Class AbstractAddress
 * @package BelVG\StorePickup\Model\Quote
 */
abstract class AbstractAddress implements AddressInterface
{
    const PICKUP_CARRIER_CODE = 'belvg_storepickup_belvg_storepickup';

    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;

    /**
     * Address constructor.
     *
     * @param \Magento\Checkout\Model\Session             $checkoutSession
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @return \Magento\Quote\Api\Data\CartInterface|\Magento\Quote\Model\Quote
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getQuote()
    {
        return $this->checkoutSession->getQuote();
    }
}
