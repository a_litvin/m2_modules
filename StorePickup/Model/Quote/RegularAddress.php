<?php
namespace BelVG\StorePickup\Model\Quote;

/**
 * Class RegularAddress
 * @package BelVG\StorePickup\Model\Quote
 */
class RegularAddress extends AbstractAddress
{
    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function update()
    {
        $regularAddress = $this->getRegularAddress();
        $quote = $this->getQuote();
        $quote->getShippingAddress()->addData(
            $regularAddress
        );
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getRegularAddress()
    {
        $billingAddress = $this->getQuote()->getBillingAddress();
        return [
            'firstname' => $billingAddress->getFirstname(),
            'lastname' => $billingAddress->getLastname(),
            'telephone' => $billingAddress->getTelephone(),
            'country_id' => $billingAddress->getCountryId(),
            'postcode' => $billingAddress->getPostcode(),
            'city' => $billingAddress->getCity(),
            'street_line1' => $billingAddress->getStreetLine(1),
            'street_line2' => $billingAddress->getStreetLine(2),
            'street' => $billingAddress->getStreet()
        ];
    }
}
