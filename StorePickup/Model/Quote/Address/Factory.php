<?php
namespace BelVG\StorePickup\Model\Quote\Address;

/**
 * Class Factory
 * @package BelVG\StorePickup\Model\Quote\Address
 */
class Factory
{
    const DEFAULT_ADDRESS_MODEL_CODE = 'regular';

    /**
     * @var array
     */
    private $addressUpdatePool;

    /**
     * Factory constructor.
     *
     * @param array $addressUpdatePool
     */
    public function __construct(
      $addressUpdatePool = []
    ) {
        $this->addressUpdatePool = $addressUpdatePool;
    }

    /**
     * @param $carrierCode
     *
     * @return \BelVG\StorePickup\Model\Quote\AddressInterface
     */
    public function getModel($carrierCode)
    {
        if (isset ($this->addressUpdatePool[$carrierCode])) {
            return $this->addressUpdatePool[$carrierCode];
        }
        return $this->addressUpdatePool[self::DEFAULT_ADDRESS_MODEL_CODE];
    }
}
