<?php
namespace BelVG\StorePickup\Model;

use BelVG\StorePickup\Model\ConfigDataInterface;

/**
 * Class ConfigData
 * @package BelVG\StorePickup\Model
 */
class ConfigData implements ConfigDataInterface
{
    /**
     * ConfigData constructor.
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getPickupAddress():array
    {
        $name = explode(' ', $this->getConfig('store_name'));
        $firstName = $name[0];
        unset($name[0]);
        $lastName = implode($name);

        return [
            'firstname' => $firstName,
            'lastname' => $lastName,
            'telephone' => $this->getConfig('store_phone'),
            'country_id' => $this->getConfig('store_country_id'),
            'postcode' => $this->getConfig('store_postcode'),
            'city' => $this->getConfig('store_city'),
            'street_line1' => $this->getConfig('store_street_line1'),
            'street_line2' => $this->getConfig('store_street_line2'),
            'street' => [
                $this->getConfig('store_street_line1'),
                $this->getConfig('store_street_line2')
            ]
        ];
    }

    /**
     * @return string
     */
    public function getStoreRegionId()
    {
        return $this->getConfig('store_region_id');
    }

    /**
     * @return bool
     */
    public function isDisplayAllRegions():bool
    {
        return $this->scopeConfig->isSetFlag(
            ConfigDataInterface::CONFIG_XML_REGION_DISPLAY,
            \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT
        );
    }

    /**
     * @param $configPath
     *
     * @return string
     */
    public function getConfig($configPath)
    {
        return (string)$this->scopeConfig->getValue(
            ConfigDataInterface::CONFIG_XML_SP_PATH_PREFIX . $configPath,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
