<?php
namespace BelVG\StorePickup\Plugin;

use BelVG\StorePickup\Model\Quote\Address;
/**
 * Class UpdateShippingAddress
 * @package BelVG\StorePickup\Plugin
 */
class UpdateShippingAddress
{
    /**
     * @var Address\Factory
     */
    private $quoteModifiersFactory;

    /**
     * UpdateShippingAddress constructor.
     *
     * @param \Magento\Checkout\Model\CompositeConfigProvider $config
     */
    public function __construct(
        \BelVG\StorePickup\Model\Quote\Address\Factory $quoteModifiersFactory
    ) {
        $this->quoteModifiersFactory = $quoteModifiersFactory;
    }

    /**
     * @param \Magento\Paypal\Model\Express\Checkout $expCheckout
     * @param                                        $methodCode
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function beforeUpdateShippingMethod(
        \Magento\Paypal\Model\Express\Checkout $expCheckout,
        $methodCode
    ) {
        $this->quoteModifiersFactory->getModel($methodCode)->update();
    }
}
