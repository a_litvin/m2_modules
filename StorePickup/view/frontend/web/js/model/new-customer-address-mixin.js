/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_StorePickup
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

define([
    'mage/utils/wrapper'
], function (wrapper) {
    'use strict';

    return function (addressFunction) {
        return wrapper.wrap(addressFunction, function (originalAddressFunction, addressData) {
            let address = originalAddressFunction(addressData);

            if (addressData.belvg_storepickup) {
                address.getType = wrapper.wrapSuper(address.getType, function () {
                    return 'storepickup-store-address';
                });
                address.isEditable = wrapper.wrapSuper(address.isEditable, function () {
                    return false;
                });
            }

            return address;
        });
    };
});
