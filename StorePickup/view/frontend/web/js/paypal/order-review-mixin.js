define([
    'jquery'
], function ($) {
    'use strict';
    return function (widget) {
        $.widget('mage.orderReview', widget, {
            /** reload page to update shipping data block **/
            _ajaxComplete: function () {
                location.reload();
            }
        });

        return $.mage.orderReview;
    }
});
