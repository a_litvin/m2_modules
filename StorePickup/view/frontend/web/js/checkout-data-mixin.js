/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_StorePickup
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

define([
    'jquery',
    'Magento_Customer/js/customer-data',
    'jquery/jquery-storageapi'
], function ($, storage) {
    'use strict';

    var cacheKey = 'checkout-data',

    /**
     * @param {Object} data
     */
    saveData = function (data) {
        storage.set(cacheKey, data);
    },

    /**
     * @return {*}
     */
    initData = function () {
        return {
            'storeCustomerShippingAddress': null
        };
    },

    /**
     * @return {*}
     */
    getData = function () {
        var data = storage.get(cacheKey)();

        if ($.isEmptyObject(data)) {
            data = $.initNamespaceStorage('mage-cache-storage').localStorage.get(cacheKey);

            if ($.isEmptyObject(data)) {
                data = initData();
                saveData(data);
            }
        }

        return data;
    };

    return function (checkoutData) {
        checkoutData.setStoreCustomerShippingAddress = function (data) {
            var obj = getData();

            obj.storeCustomerShippingAddress = data;
            saveData(obj);
        };

        checkoutData.getStoreCustomerShippingAddress = function () {
            return getData().storeCustomerShippingAddress;
        };

        return checkoutData;
    };
});
