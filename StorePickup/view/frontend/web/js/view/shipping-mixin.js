/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_StorePickup
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

define(
    [
        'jquery',
        'knockout',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/action/create-shipping-address',
        'Magento_Checkout/js/action/select-shipping-address',
        'Magento_Checkout/js/action/set-shipping-information',
        'Magento_Customer/js/model/address-list',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/quote',
        'uiRegistry'
    ],
    function (
        $,
        ko,
        checkoutData,
        createShippingAddressAction,
        selectShippingAddressAction,
        setShippingInformationAction,
        addressList,
        customer,
        quote,
        registry
    ) {
        'use strict';

        var mixin = {
            amazonLoginStatus: false,
            initialize: function () {
                var self = this;

                this._super();
                this.saveStoreAddress();
                this.isSelected.subscribe(function (value) {
                    if (value == 'belvg_storepickup_belvg_storepickup') {
                        addressList().some(function (address) {
                            if (address.getKey() == 'storepickup-store-address') {
                                selectShippingAddressAction(address);
                                return true;
                            }
                        });
                        self.isNewAddressAdded(true);
                    } else {
                        if (!self.amazonLoginStatus) {
                            addressList().some(function (address) {
                                if (address.getKey() != 'storepickup-store-address') {
                                    selectShippingAddressAction(address);
                                    return true;
                                }
                            });
                            self.isNewAddressAdded(false);
                        } else {
                            registry.get('checkout.steps.shipping-step.shippingAddress.before-form.amazon-widget-address',
                                function (component) {
                                    component.getShippingAddressFromAmazon();
                                });
                        }
                    }
                });

                registry.get('checkout.steps.billing-step.payment.payments-list.amazon_payment',
                    function (component) {
                        self.amazonLoginStatus = component.isAmazonAccountLoggedIn();
                        component.isAmazonAccountLoggedIn.subscribe(function (loggedIn) {
                            self.amazonLoginStatus = loggedIn;
                        });
                    });
            },
            saveStoreAddress: function () {
                if (!checkoutData.getStoreCustomerShippingAddress()) {
                    let addressData = window.checkoutConfig.storepickup_store_address;
                    createShippingAddressAction(addressData);
                    checkoutData.setStoreCustomerShippingAddress(addressData);
                } else {
                    createShippingAddressAction(checkoutData.getStoreCustomerShippingAddress());
                }
            },
            validateShippingInformation: function () {
                let shippingMethod = quote.shippingMethod(),
                    loginFormSelector = 'form[data-role=email-with-possible-login]',
                    emailValidationResult = customer.isLoggedIn(),
                    codeShipping;

                if (!shippingMethod ||
                    !shippingMethod['method_code'] ||
                    !shippingMethod['carrier_code']
                ) {
                    return this._super();
                }

                codeShipping = shippingMethod['method_code'] + '_' + shippingMethod['carrier_code'];
                if (!this.isFormInline || codeShipping != 'belvg_storepickup_belvg_storepickup') {
                    return this._super();
                }

                if (!customer.isLoggedIn()) {
                    $(loginFormSelector).validation();
                    emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
                }

                if (!emailValidationResult) {
                    $(loginFormSelector + ' input[name=username]').focus();

                    return false;
                }

                addressList().some(function (address) {
                    if (address.getKey() == 'storepickup-store-address') {
                        selectShippingAddressAction(address);
                        return true;
                    }
                });

                return true;
            }
        };

        return function (target) {
            return target.extend(mixin);
        };
    }
);
