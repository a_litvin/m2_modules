/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_StorePickup
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

define([
    'Magento_Checkout/js/model/quote',
    'ko'
], function (quote, ko) {
    'use strict';

    var mixin = {
        defaults: {
            template: 'BelVG_StorePickup/amzn-checkout-widget-address'
        },

        initObservable: function () {
            this._super();
            this.isVisible = ko.computed(function () {
                let shippingMethod = quote.shippingMethod(), codeShipping = '';
                if (shippingMethod) {
                    codeShipping = shippingMethod.carrier_code + '_' + shippingMethod.method_code;
                }

                return codeShipping === 'belvg_storepickup_belvg_storepickup';
            });

            return this;
        }
    };

    return function (target) {
        return target.extend(mixin);
    };
});
