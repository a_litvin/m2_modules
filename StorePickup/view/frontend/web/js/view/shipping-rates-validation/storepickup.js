/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_StorePickup
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

define([
    'uiComponent',
    'Magento_Checkout/js/model/shipping-rates-validator',
    'Magento_Checkout/js/model/shipping-rates-validation-rules',
    '../../model/shipping-rates-validator/storepickup',
    '../../model/shipping-rates-validation-rules/storepickup'
], function (
    Component,
    defaultShippingRatesValidator,
    defaultShippingRatesValidationRules,
    storepickupShippingRatesValidator,
    storepickupShippingRatesValidationRules
) {
    'use strict';

    defaultShippingRatesValidator.registerValidator('storepickup', storepickupShippingRatesValidator);
    defaultShippingRatesValidationRules.registerRules('storepickup', storepickupShippingRatesValidationRules);

    return Component;
});