/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_StorePickup
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

define(
    [
        'knockout',
        'Magento_Checkout/js/model/quote'
    ],
    function (
        ko,
        quote
    ) {
        'use strict';

        var mixin = {
            initObservable: function () {
                this._super();
                this.isVisible =  ko.computed(function () {
                    let shippingMethod = quote.shippingMethod(), codeShipping;
                    if (shippingMethod) {
                        codeShipping = shippingMethod.carrier_code + '_' + shippingMethod.method_code;
                    }

                    if (codeShipping == 'belvg_storepickup_belvg_storepickup') {
                        if (this.address().getKey() == 'storepickup-store-address') {
                            return true;
                        }
                        return false;
                    } else {
                        if (this.address().getKey() != 'storepickup-store-address') {
                            return true;
                        }
                        return false;
                    }
                }, this);

                return this;
            }
        };

        return function (target) {
            return target.extend(mixin);
        };
    }
);
