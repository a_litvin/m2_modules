/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_StorePickup
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

define([
    'Magento_Checkout/js/view/shipping-address/list',
    'Magento_Customer/js/model/address-list',
    'Amazon_Payment/js/model/storage',
    'Magento_Checkout/js/model/quote',
    'ko'
], function (Component, addressList, amazonStorage, quote, ko) {
    'use strict';

    var mixin = {
        initObservable: function () {
            this._super();
            this.visible = ko.computed(function () {
                let shippingMethod = quote.shippingMethod(), codeShipping = '';
                if (shippingMethod) {
                    codeShipping = shippingMethod.carrier_code + '_' + shippingMethod.method_code;
                }

                return addressList().length > 0 && !amazonStorage.isAmazonAccountLoggedIn() || codeShipping === 'belvg_storepickup_belvg_storepickup';
            });

            return this;
        }
    };

    return function (target) {
        return target.extend(mixin);
    };
});
