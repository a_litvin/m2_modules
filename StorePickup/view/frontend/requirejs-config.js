/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_StorePickup
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/view/shipping': {
                'BelVG_StorePickup/js/view/shipping-mixin': true
            },
            'Magento_Paypal/js/order-review': {
                'BelVG_StorePickup/js/paypal/order-review-mixin': true
            },
            'Magento_Checkout/js/model/new-customer-address': {
                'BelVG_StorePickup/js/model/new-customer-address-mixin': true
            },
            'Magento_Checkout/js/checkout-data': {
                'BelVG_StorePickup/js/checkout-data-mixin': true
            },
            'Magento_Checkout/js/view/shipping-address/address-renderer/default': {
                'BelVG_StorePickup/js/view/shipping-address/address-renderer/default-mixin': true
            },
            'Amazon_Payment/js/view/checkout-widget-address': {
                'BelVG_StorePickup/js/view/amzn-checkout-widget-address-mixin': true
            },
            'Amazon_Payment/js/view/shipping-address/list': {
                'BelVG_StorePickup/js/view/shipping-address/amzn-list-mixin': true
            }
        }
    },
    map: {
        '*': {
            'Magento_Checkout/template/shipping-address/address-renderer/default.html': 'BelVG_StorePickup/template/shipping-address/address-renderer/default.html',
            'Magento_Checkout/template/shipping-address/form.html': 'BelVG_StorePickup/template/shipping-address/form.html',
        }
    }
}
